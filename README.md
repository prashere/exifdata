A simple client side demonstration for EXIF (EXchangeable Image file Format) metadata on JPEG images.

The software that control the camera of smartphones, laptops or any DSR/DLSR digital 
cameras comes with a tool called EXIF writer. EXIF defines additional data about the 
picture or document itself which we call metadata.

Metadata by itself is not anything bad. But depends on what data and how much is 
embedded with the picture or document, it can be potentialy harmful violating the user's
privacy and can at times reveal the location, device model, date time, etc., the picture
was taken.

What's more dangerous is some manufacturers used their own proprietary formats to
embed such metadata leaving the users clueless what their devices reveal about themselves.

Using [exif-js](https://github.com/exif-js/exif-js) library, I have built this client side
demonstration of what our picture can reveal about ourselves.

You can access the [demo here](https://prashere.gitlab.io/exifdata/)

(or)

you can clone / download this repository and use in your computer itself without
depending on the demo link.

This demonstration is under a Free Software License. Therefore, feel free to fork,
modify, share and re-share under the terms of LICENSE file.