(function() {
    var file_tag = document.getElementById('file');
    basic_table = document.getElementById('basic');
    detailed_table = document.getElementById('detailed');

    file_tag.onchange = function(e) {
        basic_table.innerHTML = "";
        detailed_table.innerHTML = "";
        reader = new FileReader();
        reader.onload = function(a) {
            document.getElementById('image').src = a.target.result;
            document.getElementById('more').style.display = "block";
        }
        reader.readAsDataURL(file_tag.files[0]);

        EXIF.getData(e.target.files[0], function() {
            var make = EXIF.getTag(this, "Make");
            var model = EXIF.getTag(this, "Model");
            var datetime = EXIF.getTag(this, "DateTime");
            var flash = EXIF.getTag(this, "Flash");
            var gps_lat_ref = EXIF.getTag(this, "GPSLatitudeRef");
            var gps_lat = EXIF.getTag(this, "GPSLatitude");
            var gps_long_ref = EXIF.getTag(this, "GPSLongitudeRef");
            var gps_long = EXIF.getTag(this, "GPSLongitude");
            var location_desc = null;

            // converting DMS into DD format
            if (gps_lat !== undefined && gps_long !== undefined) {
                gps_lat = gps_lat[0] + ((gps_lat[1] + gps_lat[2]/60)/60);
                gps_long = gps_long[0] + ((gps_long[1] + gps_long[2]/60)/60);
            } else {
                gps_long = gps_lat = "No Geo Location Found in Metadata";
            }

            basic_table.innerHTML = "<tr><td><strong>Device Manufacturer</strong></td><td>" + make + 
                "</td></tr><tr><td><strong>Model</strong></td><td>" + model +
                "</td></tr><tr><td><strong>Captured On:</strong></td><td>" + datetime +
                "</td></tr><tr><td><strong>Flash Used?</strong></td><td>" + flash +
                "</td></tr><tr><td><strong>GPS Latitude</strong></td><td>" + gps_lat + 
                "</td></tr><tr><td><strong>GPS Longitude</strong></td><td>" + gps_long + "</td></tr>";
            
            exif_tags = EXIF.getAllTags(this);
            for (tag in exif_tags){
                if (tag !== "MakerNote" && tag !== "undefined") {
                    tr = document.createElement("tr");
                    td1 = document.createElement("td");
                    td2 = document.createElement("td");

                    td1.innerHTML = tag;
                    tr.appendChild(td1);

                    td2.innerHTML = exif_tags[tag];
                    tr.appendChild(td2); 
                    detailed_table.appendChild(tr);
                }
            }
        });
    }
})();
